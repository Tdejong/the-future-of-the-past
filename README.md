# how to start the website

for the backend go to the backend folder and run these commands:

- npm i, to install all the neccesary packages
- npm run dev, to run de backend

for the frontend go to the frontend folder and run these:

- npm i, to install all the neccesary packages 
- npm run serve, to run de backend

make sure you have mysql and mysql workbench and import the data stored in the backend (sql_db.sql) with the workbench.

if the backend is delivered without data (3d scans, video's, tumbnails) then make sure
you truncate all the tables in database with the MYSQL workbench, but DON'T DELETE THEM.

the website should now be able to run without error.


