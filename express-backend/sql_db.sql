-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: pot_db
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jar`
--

DROP TABLE IF EXISTS `jar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jar` (
  `id` int NOT NULL AUTO_INCREMENT,
  `periodName` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `threeD` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `omvang_pot` varchar(255) DEFAULT NULL,
  `versiering` mediumtext,
  `datering` mediumtext,
  `vindplaats` mediumtext,
  `vorm` mediumtext,
  `titel` mediumtext,
  PRIMARY KEY (`id`),
  KEY `periodName` (`periodName`),
  CONSTRAINT `jar_ibfk_1` FOREIGN KEY (`periodName`) REFERENCES `period` (`periodName`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jar`
--

LOCK TABLES `jar` WRITE;
/*!40000 ALTER TABLE `jar` DISABLE KEYS */;
INSERT INTO `jar` VALUES (1,'neolithicum','/assets/potten/image0.jpg','/assets/potten/pot0.obj',NULL,'7,7 cm bij 8,9cm','Op deze trechterbeker zit geen versiering. In de binnenkant van de pot zit ook geen versiering.','Midden-Neolithicum B (3400 - 2850 v.Chr.) Onderdeel van de Trechterbekercultuur.','Baalder es','De trechtbekers zijn meestal glad of gepolijst en is gemagerd met kwarts of zand. Deze trechtbeker kom heeft een vlakke bodem.','POM-196:	Trechterbeker onversierd'),(2,'neolithicum','/assets/potten/image1.jpg','/assets/potten/pot1.obj',NULL,'20 cm bij 13,4 cm','De versiering op deze maritieme klokbeker is afwisselende versierde en onversierde zones met horizontale kamlijnen. Deze versiering is over de gehele pot. In de binnenkant van de pot zit geen versiering.','Laat-Neolithicum B (2450 - 2000 v.Chr.) Onderdeel van de Klokbekercultuur.','Putten','De klokbekers zijn dunwandig bekeraardewerk opgebouwd uit rollen klei gemagerd met brokjes aardewerk, zand en fijn grind. De maritieme klokbeker heeft een s-vormig profiel met een uitlopende hals. Het heeft een vlakke bodem.','RMT-1979-3:	Maritieme Klokbeker'),(3,'bronzeAge','/assets/potten/image2.jpg','/assets/potten/pot2.obj',NULL,'10,8 cm bij 11,6 cm','Onder de dikkere uitstaande rand zit een rij van vingertopindrukken en boven de scherpe buikknik zit ook een rij van vingertopindrukken. In de binnenkant van de pot zit ook geen versiering.','Midden-Bronstijd - Late-IJzertijd (1100 - 12 v.Chr.)','De Zandhorst','Deze urn heeft een slanke bodem met een scherpe buikknik en een iets naar buiten uitstaande dikkere rand. De urn heeft ook een oortje bij de scherpe buikknik.','POM-5439:	Urn'),(4,'ironAge','/assets/potten/image3.jpg','/assets/potten/pot3.obj',NULL,'10,8 cm bij 11,6 cm','Onder de dikkere uitstaande rand zit een rij van vingertopindrukken en boven de scherpe buikknik zit ook een rij van vingertopindrukken. In de binnenkant van de pot zit ook geen versiering.','Midden-Bronstijd - Late-IJzertijd (1100 - 12 v.Chr.)','De Zandhorst','Deze urn heeft een slanke bodem met een scherpe buikknik en een iets naar buiten uitstaande dikkere rand. De urn heeft ook een oortje bij de scherpe buikknik.','POM-5439:	Urn'),(5,'neolithicum','/assets/potten/image4.jpg','/assets/potten/pot4.obj',NULL,'9.5 cm bij 17 cm','De kom heeft versiering op de schouder en buik. Op de schouder zitten horizontale blokken van vier diepsteeklijnen. Op de buik zitten verticale blokken van vier diepsteeklijnen. De verticale en horizontale blokken gaan rondom de gehele pot. In deze diepsteeklijnen heeft witte bot pasta gezeten. Dit is niet meer zichtbaar op de 3D scan. Op de hals van de kom zijn twee rijen van vingertopindrukken In de binnenkant van de pot zit geen versiering.','Midden-Neolithicum B (3400 - 2850 v.Chr.) Onderdeel van de Trechterbekercultuur.','Hordenberg','De trechtbekers zijn meestal glad of gepolijst en is gemagerd met kwarts of zand. Deze trechtbeker kom heeft een vlakke bodem.','POM-166:	Trechterbeker kom'),(6,'neolithicum','/assets/potten/image5.jpg','/assets/potten/pot5.obj',NULL,'13,9 bij 7,1 cm','Deze standvoetbeker is versierd van de hals tot aan de buik. Op de hals zijn horizontale rijen van touwlijnen te zien. Op de buik is een rij aanwezig van verticale touwlijnen. In de binnenkant van de pot  zit geen versiering.','Laat-Neolithicum B (2450 - 2000 v.Chr.). Onderdeel van de Enkelgrafcultuur.','Ermelo','De standvoetbekers zijn dunwandig en zijn gemagerd met zand. De standvoetbeker heeft een S-profiel en een langgerekte vorm.','OKT-261:	Standvoetbeker '),(7,'neolithicum','/assets/potten/image6.jpg','/assets/potten/pot6.obj',NULL,'21,7 cm bij 13,1 cm.','Dit is een All Over Ornamented standvoetbeker. De hele beker is versierd. Deze pot heeft twee verschillende versieringstechnieken. Op deze pot is vissengraat versiering en touwlijnen te zien. Het wisselt elkaar af. Telkens 3 rijen van vissengraat en dan drie rijen van touwlijnen over de gehele pot. In de binnenkant van de pot zit geen versiering.','Laat-Neolithicum B (2450 - 2000 v.Chr.). Onderdeel van de Enkelgrafcultuur.','Ermelo','De standvoetbekers zijn dunwandig en zijn gemagerd met zand. De standvoetbeker heeft een S-profiel en een langgerekte vorm.','OKT-250: 	Standvoetbeker'),(8,'bronzeAge','/assets/potten/image7.jpg','/assets/potten/pot7.obj',NULL,'14,2 cm bij 10,7 cm','De hals en buik van deze pot zijn versierd met een WKD-stempel in verticale lijnen. Op de bodem van de pot zit een aantal verticale rijen van vingertopindrukken. In de binnenkant van de pot zit geen versiering.','Vroege-Bronstijd (2000 - 1800 v.Chr.). Onderdeel van de Wikkeldraadcultuur, overeenkomend met het late Wikkeldraad aardewerk.','Ermelo','Het late WKD-aardewerk is dunwandig en is gemagerd met steengruis. Dit varieerde tussen fijn en grof steengruis. Dit aardewerk heeft een eivormig lichaam met een hoge hals insnoering. Dit aardewerk staat bekend om de smalle voet met uitstaande hals.','OKT-236:	Wikkeldraad'),(9,'neolithicum','/assets/potten/image8.jpg','/assets/potten/pot8.obj',NULL,'9.5 cm bij 17 cm','De kom heeft versiering op de schouder en buik. Op de schouder zitten horizontale blokken van vier diepsteeklijnen. Op de buik zitten verticale blokken van vier diepsteeklijnen. De verticale en horizontale blokken gaan rondom de gehele pot. In deze diepsteeklijnen heeft witte bot pasta gezeten. Dit is nog steeds zichtbaar op de 3D scan. Op de hals van de kom is een horizontale groeflijn. In de binnenkant van de pot zit geen versiering.','Midden-Neolithicum B (3400 - 2850 v.Chr.) Onderdeel van de Trechterbekercultuur.','Baalder es','De trechtbekers zijn meestal glad of gepolijst en gemagerd met kwarts of zand. Deze trechtbeker kom heeft een vlakke bodem.','POM-167:	Trechterbeker kom'),(10,'bronzeAge','/assets/potten/image9.jpg','/assets/potten/pot9.obj',NULL,'17,5 cm bij 16.9cm','Op de urn zit geen versiering.','Midden-Bronstijd - Late-IJzertijd (1100 - 12 v.Chr.)','De Zandhorst','Deze urn is dunwandig. Het heeft een slanke bodem met een bollere bovenkant een conische hals. Het oortje zit op de grootste buikomvang.','OKT-040:	Urn met oor'),(11,'ironAge','/assets/potten/image10.jpg','/assets/potten/pot10.obj',NULL,'17,5 cm bij 16.9cm','Op de urn zit geen versiering.','Midden-Bronstijd - Late-IJzertijd (1100 - 12 v.Chr.)','De Zandhorst','Deze urn is dunwandig. Het heeft een slanke bodem met een bollere bovenkant een conische hals. Het oortje zit op de grootste buikomvang.','OKT-040:	Urn met oor'),(12,'bronzeAge','/assets/potten/image11.jpg','/assets/potten/pot11.obj',NULL,'16,7 cm bij 20,5 cm.','Op de urn zit geen versiering.','Midden-Bronstijd - Late-IJzertijd (1100 - 12 v.Chr.)','Onbekend','De urn is dunwandig. Het heeft een brede lage vorm met een naar buiten uitstaande rand en een scherpe buikknik.','PROV-340:	Urn A'),(13,'ironAge','/assets/potten/image12.jpg','/assets/potten/pot12.obj',NULL,'16,7 cm bij 20,5 cm.','Op de urn zit geen versiering.','Midden-Bronstijd - Late-IJzertijd (1100 - 12 v.Chr.)','Onbekend','De urn is dunwandig. Het heeft een brede lage vorm met een naar buiten uitstaande rand en een scherpe buikknik.','PROV-340:	Urn A'),(14,'bronzeAge','/assets/potten/image13.jpg','/assets/potten/pot13.obj',NULL,'10,3 cm bij 10,3 cm','Op dit aardewerk zit geen versiering.','Midden-Bronstijd (1800 - 1100 v.Chr.). Kümmerkeramik is onderdeel van de Elp-cultuur.','De Aust','Het Kümmerkeramik, is zeer eenvoudig van vorm. Het heeft een ton- en emmervorm. Voor dit aardewerk is gebruik gemaakt van grove maaksel met daarin als magering met gebroken kwarts. Het aardewerk heeft dikke randen en een dikke bodem.','OKT-88:	Tonvormig potje'),(15,'neolithicum','/assets/potten/image14.jpg','/assets/potten/pot14.obj',NULL,'10 cm bij 10.5 cm','Op de hals van dit aardewerk zit een golvend patroon van drie horizontale lijnen. Op de buik zitten verticale diepsteeklijnen. In de binnenkant van de pot zit geen versiering.','Midden-Neolithicum B (3400 - 2850 v.Chr.) Onderdeel van de Trechterbekercultuur.','Baalder es',' De trechtbekers zijn meestal glad of gepolijst en is gemagerd met kwarts of zand. Deze trechterbeker heeft een trechtervormige hals op een bolle buik.','POM-169:	Trechterbeker'),(16,'ironAge','/assets/potten/image15.jpg','/assets/potten/pot15.obj',NULL,'18,3 bij 15,5 cm.','Op de rand zitten vingerindrukken. De rest van de pot is niet versierd. In de binnenkant van de pot zit geen versiering.','Vroege-IJzertijd (800 - 500 v.Chr.). Vermoedelijk een Harpstedtpot. Onderdeel van La Tène Cultuur ','Hulsen','Dit is dunwandig aardewerk dat is gemagerd met chamotte, steengruis of grind. Dit aardewerk heeft een hoge ronde schouder en een iets uitstekende rand. Deze pot is besmeten en is op bepaalde plekken lichter van kleur.','OKT-112:	Urn Besmeten'),(17,'neolithicum','/assets/potten/image16.jpg','/assets/potten/pot16.obj',NULL,'11,5 bij 3 cm.','Het kraaghals flesje heeft geen versiering.','Midden-Neolithicum B (3400 - 2850 v.Chr.) Onderdeel van de Trechterbekercultuur.','Baalder es','De trechtbekers zijn meestal glad of gepolijst en is gemagerd met kwarts of zand. Heeft  de vorm van een kraaghals flesje.','POM-172:	Trechterbeker Kraaghals flesje');
/*!40000 ALTER TABLE `jar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `period`
--

DROP TABLE IF EXISTS `period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `period` (
  `periodName` varchar(255) NOT NULL,
  PRIMARY KEY (`periodName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `period`
--

LOCK TABLES `period` WRITE;
/*!40000 ALTER TABLE `period` DISABLE KEYS */;
INSERT INTO `period` VALUES ('bronzeAge'),('ironAge'),('neolithicum');
/*!40000 ALTER TABLE `period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz`
--

DROP TABLE IF EXISTS `quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `quiz` (
  `id` int NOT NULL AUTO_INCREMENT,
  `onderwerp` varchar(255) NOT NULL,
  `vragen` varchar(255) NOT NULL,
  `antwoorden` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz`
--

LOCK TABLES `quiz` WRITE;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `videos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `tubmnail` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `theme` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (1,'Ijzertijd boerderij','/assets/tumbnails/tumbnail0.jpg','','/assets/videos/video0.mp4','ironAge'),(2,'Decoratietechnieken','/assets/tumbnails/tumbnail1.jpg','','/assets/videos/video1.mp4','neolithicum'),(3,'Hunnebed','/assets/tumbnails/tumbnail2.jpg','','/assets/videos/video2.mp4','neolithicum'),(4,'Trechterbeker maken','/assets/tumbnails/tumbnail3.jpg','','/assets/videos/video3.mp4','neolithicum'),(5,'Kleiwinning','/assets/tumbnails/tumbnail4.jpg','','/assets/videos/video4.mp4','neolithicum'),(6,'Hunnebedcentrum','/assets/tumbnails/tumbnail5.jpg','','/assets/videos/video5.mp4','neolithicum');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-16 13:14:58
