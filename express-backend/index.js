const express = require("express");
const dummyData = require("./db.json");
const cors = require("cors");
var bodyParser = require('body-parser')
const db = require("./database");
const fileUpload = require('express-fileupload');


const app = express();
app.use(cors());

//body parser is needed for receiving json
var jsonParser = bodyParser.json()
const path = require("path");
const { send } = require("process");

// enable files upload
app.use(fileUpload());

//returns a video
app.use("/assets", express.static(path.join(__dirname, "assets")));


app.post('/uploadVideo', async (req, res) =>{
  //TODO: add security, a user could send a empty form
  if(req.files){
    let body = req.body

    //fetch the lengt of the database and prepare an ID code 
    const videoIdQuery = await db.promise().query("SELECT COUNT(id) AS idCode FROM videos;")
    const videoId = videoIdQuery[0][0].idCode

    //prepare the files 
    const avatar = req.files;
    //fetch the tumbnail
    const tumbnail = avatar.tumbnail
    //fetch the video
    const video = avatar.video

    //try and place the tumbnail+video in the correct folders
    //with the correct name based of of the ID
    try{
      tumbnail.mv('./assets/tumbnails/tumbnail' + videoId + '.jpg' )
      video.mv('./assets/videos/video' + videoId + '.mp4')
    }catch(e){
      console.log(e)
    } 

    //prevents small bugs idk y
    const description = body.description

    //insert the new video in the database so the user can see it
    const newVideo = await db.promise().query(`INSERT INTO videos(title,  description, tubmnail, route, theme)
    VALUES ('${body.title}', '${description}', '/assets/tumbnails/tumbnail${videoId}.jpg', '/assets/videos/video${videoId}.mp4', '${body.theme}');`)

    res.sendStatus(200)
  }
})

app.post("/video" ,jsonParser, async (req, res) => {
  const { title, description, tumbnail, video, theme } = req.body;
  await db.promise()
  .query(`INSERT INTO videos(title, description, route, theme)
  VALUES ('${title}', '${description}',
  '/assets/video3.mp4', '${theme}');`);
  res.send("Post request made");

});

app.get("/video", async (req, res) => {
  const result = await db.promise().query(`SELECT * FROM videos`);
  res.send(result);
});

app.get("/tumbnail/:id", async (req, res) => {
  const id = req.params.id;
  const result = await db
    .promise()
    .query(`SELECT tubmnail FROM videos WHERE id = ${id}`);
  const tumbnail = result[0][0].tubmnail;

  res.sendFile(__dirname + tumbnail);
});

app.get("/video/:id", async (req, res) => {
  const id = req.params.id;
  const result = await db
    .promise()
    .query(`SELECT route FROM videos WHERE id = ${id}`);
  const route = result[0][0].route;
  res.sendFile(__dirname + route);
});

app.get("/videos/:theme", async (req, res) => {
  const theme = req.params.theme;
  const result = await db.promise().query(`SELECT * FROM videos WHERE theme = '${theme}' `);

  res.send(result)
});

app.get("/", (req, res) => {
  res.send(dummyData);
});

app.get("/facts", (req, res) => {
  res.send(dummyData.facts);
});


//Get all jars from this period.
app.get('/:period/jars', async (req, res) => {
  const period = req.params.period
  const result = await db.promise().query(`SELECT id, titel FROM jar WHERE periodName LIKE '%${period}%'`);
  res.send(result[0])
})

app.get('/:period/jars/:id', async (req, res) => {
  const period = req.params.period
  const id = req.params.id
  const result = await db.promise().query(`SELECT * FROM jar WHERE id=${id} AND periodName LIKE '%${period}%'`);
  if(result[0][0] === undefined){
    res.sendStatus(404)
  }else{
    res.send(result[0][0])
  }
})

app.get('/:period/jars/:id/image', async (req, res) => {
  const period = req.params.period
  const id = req.params.id

  const result = await db.promise().query(`SELECT * FROM jar WHERE id=${id} AND periodName LIKE '%${period}%'`);
  const url = result[0][0].image

  res.sendFile(__dirname + url);
})



app.get('/:period/jars/:id/3d', async (req, res) => {
  const period = req.params.period
  const id = req.params.id

  const result = await db.promise().query(`SELECT * FROM jar WHERE id=${id}`);
  const url = result[0][0].threeD

  res.sendFile(__dirname + url);
})


app.post('/uploadJar', async (req, res) => {
  // if(!files){
  const body = req.body

  const jarIdQuery = await db.promise().query("SELECT COUNT(id) AS idCode FROM jar;")
  const jarId = jarIdQuery[0][0].idCode

  //prepare the files 
  const avatar = req.files;
  //fetch the image
  const image = avatar.image
  //fetch the OBJ file
  const objFile = avatar.obj

  let video = avatar.video

  try{
    image.mv('./assets/potten/image' + jarId + '.jpg' )
    objFile.mv('./assets/potten/pot' + jarId + '.obj')
    
    // const newJar = await db.promise().query(`
    // INSERT INTO jar(periodName, titel, image, threeD, video)
    // VALUES('${body.period}', '${body.title}', 
    // '/assets/potten/image${jarId}.jpg',
    // '/assets/potten/pot${jarId}.obj', 
    // '/assets/potten/video${jarId}.mp4');
    // `)

    if(video){
      video.mv('./assets/potten/video' + jarId + '.mp4')
      
    }
  }catch(e){
    console.log(e)
  } 


    const newJar = await db.promise().query(`
    INSERT INTO jar(periodName, titel, image, threeD, datering, vorm, vindplaats, omvang_pot, versiering)
    VALUES('${body.period}', '${body.title}', '/assets/potten/image${jarId}.jpg', '/assets/potten/pot${jarId}.obj', '${body.datering}', '${body.vorm}', '${body.vindplaats}', '${body.omvang_pot}', '${body.versiering}');
    `)

  //}
  res.send(200)
})

app.get('/quiz', async (req, res) => {
  const result = await db.promise().query(`SELECT * FROM quiz`);
  res.send(result[0])
})

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`server listing on port ${PORT}`));
