import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Archive from '../views/Archive.vue'
import Videos from '../views/Videos.vue'
import AddPage from '../views/AddPage.vue'
import NotFound from '../views/NotFound.vue'
import Login from '../views/Login.vue'

import Watch3D from '../components/Watch3D.vue'
import Pottery from '../components/Pottery.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/archive',
    name: 'Archive',
    component: Archive
  },
  {
    path: '/videos',
    name: 'Videos',
    component: Videos
  },
  {
    path: '/AddPage',
    name: 'AddPage',
    component: AddPage
  },
  {
    path: '/:age/pottery/:id',
    name: 'Pottery',
    props: true,
    component: Pottery,
  },
  {
    path: '/:age/pottery/:id/3D',
    name: '3D',
    props: true,
    component: Watch3D
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  // catchall 404
  {
    path: '/:catchAll(.*)',
    name: 'NotFound',
    component: NotFound
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
